<?php

namespace harlam\GsmModem;

class COPSCommandResult
{
    private $_data;

    public function __construct(array $data)
    {
        foreach ($data as $line) {
            $line = str_replace("\r\n", '', $line);
            $exploded = explode(':', $line, 2);
            if (isset($exploded[1]) && !empty($exploded[1])) {
                $this->_data[$exploded[0]] = trim($exploded[1]);
            }
        }
    }

    public function getMobileOperatorName(): string
    {
        $result = key_exists('+COPS', $this->_data) ? $this->_data['+COPS'] : '';
        return str_replace('"', '', (explode(',', $result))[2]);
    }
}