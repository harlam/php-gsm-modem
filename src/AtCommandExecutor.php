<?php

namespace harlam\GsmModem;

use harlam\GsmModem\Exceptions\AtCommandErrorException;
use harlam\GsmModem\Interfaces\iAtCommandExecutor;
use lepiaf\SerialPort\Configure\TTYConfigure;
use lepiaf\SerialPort\Parser\SeparatorParser;
use lepiaf\SerialPort\SerialPort;

class AtCommandExecutor implements iAtCommandExecutor
{
    private $_serialPort;

    private $_linefeed = "\r\n";

    public function __construct(string $modem)
    {
        $this->_serialPort = new SerialPort(new SeparatorParser(), new TTYConfigure());
        $this->_serialPort->open($modem);
    }

    public function __destruct()
    {
        $this->_serialPort->close();
    }

    /**
     * @param string $command
     * @throws AtCommandErrorException
     * @return array
     */
    public function doCommand(string $command): array
    {
        $result = [];
        $this->_serialPort->write($command . $this->_linefeed);
        while ($data = $this->_serialPort->read()) {
            if (substr($data, 0, 2) === 'OK') {
                break;
            } elseif (substr($data, 0, 5) === 'ERROR') {
                throw new AtCommandErrorException('ERROR');
            }
            $result[] = $data;
        }

        return $result;
    }

    /**
     * Символ перевода строки
     * @param string $lf
     */
    public function setLinefeed(string $lf): void
    {
        $this->_linefeed = $lf;
    }
}