<?php

namespace harlam\GsmModem;

class AtiCommandResult
{
    private $_data;

    public function __construct(array $data)
    {
        foreach ($data as $line) {
            $line = str_replace("\r\n", '', $line);
            $exploded = explode(':', $line, 2);
            if (isset($exploded[1]) && !empty($exploded[1])) {
                $this->_data[$exploded[0]] = trim($exploded[1]);
            }
        }
    }

    public function getManufacturer(): string
    {
        return key_exists('Manufacturer', $this->_data) ? $this->_data['Manufacturer'] : '';
    }

    public function getModel(): string
    {
        return key_exists('Model', $this->_data) ? $this->_data['Model'] : '';
    }

    public function getRevision(): string
    {
        return key_exists('Revision', $this->_data) ? $this->_data['Revision'] : '';
    }

    public function getImei(): string
    {
        return key_exists('IMEI', $this->_data) ? $this->_data['IMEI'] : '';
    }

    public function getGcap(): string
    {
        return key_exists('+GCAP', $this->_data) ? $this->_data['+GCAP'] : '';
    }
}