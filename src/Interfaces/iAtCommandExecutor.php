<?php

namespace harlam\GsmModem\Interfaces;

/**
 * Выполнение AT комманд
 * Interface iAtCommandExecutor
 */
interface iAtCommandExecutor
{
    /**
     * Список строк
     * @param string $command
     * @return array
     */
    public function doCommand(string $command): array;

    /**
     * Символ перевода строки
     * @param string $lf
     */
    public function setLinefeed(string $lf): void;
}