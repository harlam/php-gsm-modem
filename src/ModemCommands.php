<?php

namespace harlam\GsmModem;

use harlam\GsmModem\Interfaces\iAtCommandExecutor;

class ModemCommands
{
    private $_atCommandsExecutor;

    public function __construct(iAtCommandExecutor $atCommandExecutor)
    {
        $this->_atCommandsExecutor = $atCommandExecutor;
    }

    public function doAtiCommand(): AtiCommandResult
    {
        return new AtiCommandResult($this->_atCommandsExecutor->doCommand("ATI"));
    }

    public function doCOPSCommand(): COPSCommandResult
    {
        return new COPSCommandResult($this->_atCommandsExecutor->doCommand("AT+COPS?"));
    }

    public function doUSSDCommand(string $command): void
    {
        $this->_atCommandsExecutor->doCommand("AT+CUSD=1,{$command},15");
    }
}